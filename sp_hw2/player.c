#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<string.h>
#include<unistd.h>
#include<libgen.h>
#include<dirent.h>
#include<pwd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/time.h>
#include<sys/select.h>
#include<limits.h>
void move(int card[], int len)
{
	int i, j = 0;
	int movearray[len];
	char append[128] = "";
	for(i = 0; i < len; i++)
	{
		if(card[i] != -1)
		{
			movearray[j] = card[i];
			j++;
		}
	}
	for(i = 0; i < j; i++)
	{
		card[i] = movearray[i];
	}
	for(i = j; i < len; i++)
	{
		card[i] = -1;
	}
}
int main(int argc, char *argv[])
{
	int randKey = atoi(argv[3]);
	char playerIndex = argv[2][0];
	int judgeId = atoi(argv[1]);
	int total;
	int n;
	if(playerIndex == 'A')
		total = 14;
	else total = 13;
	int card[total];
	char buf[20] = "", bufjudge[20] = "";
	strcpy(buf, "judge");
	strcat(buf, argv[1]);
	strcpy(bufjudge, buf);
	if(playerIndex == 'A')
		strcat(buf, "_A.FIFO");
	else if(playerIndex == 'B')
		strcat(buf, "_B.FIFO");
	else if(playerIndex == 'C')
		strcat(buf, "_C.FIFO");
	else if(playerIndex == 'D')
		strcat(buf, "_D.FIFO");
	
	FILE* fplayer = fopen(buf, "r+");
	strcat(bufjudge, ".FIFO");
	FILE* fjudge = fopen(bufjudge, "r+");
	//read cards from judge
	char buffer[128];
	fgets(buffer, 128, fplayer);
	int i, j = 0, k = 0, m, count;
	if(playerIndex == 'A')
	{
		count = 14;
		for(i = 0; i < 14; i++)
		{
			char temp[128] = "";
			char append[128];
			while((buffer[j] != ' ') && (buffer[j] != '\n'))
			{
				sprintf(append, "%c", buffer[j]);
				strcat(temp, append);
				j++;
			}
			j++;
			card[i] = atoi(temp);
		}
		//eliminate cards
		for(i = 0; i < 14; i++)
		{
			for(j = i + 1; j < 14; j++)
			{
				if(card[i] == card[j])
					card[i] = card[j] = -1;
			}
		}
		for(i = 0; i < 14; i++)
		{
			if(card[i] == -1)
				count--;
		}
		move(card, 14);
	}
	else
	{
		count = 13;
		for(i = 0; i < 13; i++)
		{
			char temp[128] = "";
			char append[128];
			while((buffer[j] != ' ') && (buffer[j] != '\n'))
			{
				sprintf(append, "%c", buffer[j]);
				strcat(temp, append);
				j++;
			}
			j++;
			card[i] = atoi(temp);
		}
		//eliminate card
		for(i = 0; i < 13; i++)
		{
			for(j = i + 1; j < 13; j++)
			if(card[i] == card[j])
			{
				card[i] = card[j] = -1;
			}
		}
		for(i = 0; i < 13; i++)
			if(card[i] == -1)
				count--;
		move(card, 13);
	}
	//tell judge the number of cards
	fprintf(fjudge, "%c %d %d\n", playerIndex, randKey, count);
	fflush(fjudge);
	
	while(1){
	char BUF[128];
	fgets(BUF, 128, fplayer);
	
	char send[128];
	if(BUF[0] == '<')
	{
		//which card to draw
		BUF[0] = BUF[1] = ' ';
		int y = atoi(BUF);
		int a = rand() % y;
		//tell judge which card we want
		fprintf(fjudge, "%c %d %d\n", playerIndex, randKey, a);
		fflush(fjudge);
		
		//which card we get
		char BUFF[128];
		fgets(BUFF, 128, fplayer);
		int n = atoi(BUFF);
		int x;
		int eli;
		for(x = 0; x < total; x++)
		{
			if(card[x] == n)
			{
				card[x] = -1;
				move(card, total);
				eli = 1;
				break;
			}
		}
		if(x == total)
		for(x = 0; x < total; x++)
		{
			if(card[x] == -1)
			{
				card[x] = n;
				eli = 0;
				break;
			}
		}
		//tell judge if eliminated
		fprintf(fjudge, "%c %d %d\n", playerIndex, randKey, eli);
		fflush(fjudge);
	}
	else
	{
		//send the card be drawn
		BUF[0] = BUF[1] = ' ';
		int y = atoi(BUF);
		fprintf(fjudge, "%c %d %d\n", playerIndex, randKey, card[y]);
		fflush(fjudge);
		card[y] = -1;
		move(card, total);
	}
	}//end while
}