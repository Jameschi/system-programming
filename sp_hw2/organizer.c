#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<string.h>
#include<unistd.h>
#include<libgen.h>
#include<dirent.h>
#include<pwd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/time.h>
#include<sys/select.h>
#include<limits.h>
#define free 0
#define busy 1

char answer[100000][128];
int ret = -1;
int construct(int playerNum)
{
	int i, j, k, l, m = 0;
	char BUFFER[128];
	for(i = 1; i <= playerNum; i++)
	for(j = i + 1; j <= playerNum; j++)
	for(k = j + 1; k <= playerNum; k++)
	for(l = k + 1; l <= playerNum; l++)
	{	
		sprintf(BUFFER, "%d %d %d %d\n", i, j, k, l);
		strcpy(answer[m], BUFFER);
		m++;
	}
	sprintf(BUFFER, "%d %d %d %d\n", 0, 0, 0, 0);
	strcpy(answer[m], BUFFER);
	return m + 1;
} 
int combination(int x)
{
	return x * (x - 1) * (x - 2) * (x - 3) / 24;
}
char* deal()
{
	ret++;
	return answer[ret];
}
typedef struct ans
{
	int NO;
	int score;
}ANS;
int compare(const void *a, const void *b)
{
	ANS * i = (ANS *) a;
	ANS * j = (ANS *) b;
	if(i -> score < j -> score)
	return -1;
	else if(i -> score == j -> score)
	return i -> NO >= j -> NO;
	else return 1;
}

typedef struct fd 
{
	int fdin[2];//data from organizer to judge
	int fdout[2];//data from judge to organizer
}FD;

int main(int argc, char* argv[])
{
	int i = 0, j = 0, k = 0, w = 0;
	pid_t pid;
	int currentWork = 0;
	int judgeNum = atoi(argv[1]);
	int playerNum = atoi(argv[2]);
	FD fdPipe[judgeNum];
	int player[playerNum];
	for(i = 0; i < playerNum; i++)
		player[i] = 0;
	int judge[judgeNum];
	for(i = 0; i < judgeNum; i++)
		judge[i] = free;
	int count = 0;
	for(i = 0; i < judgeNum; i++)
	{
		count++;
		if(pipe(fdPipe[i].fdin) == -1 || pipe(fdPipe[i].fdout) == -1)   
			perror("pipe error");
		else
		{
			if((pid = fork()) == -1)
				perror("fork error");
			else if(pid == 0)//child process
			{
				close(fdPipe[i].fdin[1]);
				close(fdPipe[i].fdout[0]);
				dup2(fdPipe[i].fdout[1], STDOUT_FILENO);
				dup2(fdPipe[i].fdin[0], STDIN_FILENO);
				close(fdPipe[i].fdout[1]);
				close(fdPipe[i].fdin[0]);
				char ex[20];
				sprintf(ex, "%d", i + 1);
				if(-1 == execlp("./judge", "./judge", ex, (char*) 0))
					perror("execlp");
			}	
			else if(pid > 0)//parent process
			{
				close(fdPipe[i].fdin[0]);
				close(fdPipe[i].fdout[1]);
			}
		}
	}
	construct(playerNum);
	int com = combination(playerNum);
	fd_set work_fd;
	fd_set read_work_fd;
	FD_ZERO(&work_fd);
	FD_ZERO(&read_work_fd);
	
	for(i = 0; i < judgeNum; i++)
		FD_SET(fdPipe[i].fdout[0], &work_fd);
    int race = 0;
    int BREAK = 0, BREAK1 = 0;
	while(1)
	{
		char buffer[20];
		int x;
		char* BUF = "0 0 0 0\n";
		char tempComp[128];
		for(x = 0; x < judgeNum; x++)
		{
			if(BREAK == com)
			break;
			if(judge[x] == free)//assign job
			{
				strcpy(tempComp, deal());
				write(fdPipe[x].fdin[1], tempComp, strlen(tempComp) + 1);
				judge[x] = busy;
				BREAK++;
			}
		}
		memcpy(&read_work_fd, &work_fd, sizeof(work_fd));
		select(1024, &read_work_fd, NULL, NULL, NULL);//IO multiplexing
		for(x = 0; x < judgeNum; x++)
		{
			if(FD_ISSET(fdPipe[x].fdout[0], &read_work_fd) && (judge[x] == busy))//read data from judges
			{
				read(fdPipe[x].fdout[0], buffer, 128);
				player[atoi(buffer) - 1] -= 1;
				judge[x] = free;
				race++;
			}
			if(race == com)
			{
				BREAK1 = 1;
				break;
			}
		}
		if(BREAK1 == 1)
		{
			for(x = 0; x < judgeNum; x++)
			write(fdPipe[x].fdin[1], BUF, strlen(BUF) + 1);
			break;
		}
	}
	ANS sortarray[playerNum];
	for(i = 0; i < playerNum; i++)
	{
		sortarray[i].NO = i + 1;
		sortarray[i].score = player[i];
	}
	qsort(sortarray, playerNum, sizeof(ANS), compare);
	for(i = 0; i < playerNum; i++)
		fprintf(stderr, "%d ", sortarray[i].NO);
}
