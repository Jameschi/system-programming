#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<string.h>
#include<unistd.h>
#include<libgen.h>
#include<dirent.h>
#include<pwd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/time.h>
#include<sys/select.h>
#include<limits.h>
int KEY[4] = {1, 2, 3, 4};
char cardA[128] = "";
char cardB[128] = "";
char cardC[128] = "";
char cardD[128] = "";
int card[53];
void deal(void)
{
	int i;
	char append[128] = "";
	char carda[128] = "";
	char cardb[128] = "";
	char cardc[128] = "";
	char cardd[128] = "";
	for(i = 0; i < 13; i++)
	{
		sprintf(append, "%d ", card[i]);
		strcat(carda, append);
	}
	sprintf(append, "%d\n", card[i]);
	strcat(carda, append);
	strcpy(cardA, carda);
	for(i = 14; i < 26; i++)
	{
		sprintf(append, "%d ", card[i]);
		strcat(cardb, append);
	}
	sprintf(append, "%d\n", card[i]);
	strcat(cardb, append);
	strcpy(cardB, cardb);
	for(i = 27; i < 39; i++)
	{
		sprintf(append, "%d ", card[i]);
		strcat(cardc, append);
	}
	sprintf(append, "%d\n", card[i]);
	strcat(cardc, append);
	strcpy(cardC, cardc);
	for(i = 40; i < 52; i++)
	{
		sprintf(append, "%d ", card[i]);
		strcat(cardd, append);
	}
	sprintf(append, "%d\n", card[i]);
	strcat(cardd, append);
	strcpy(cardD, cardd);
}
void shuffle()
{
	int i, a, temp;
	card[0] = 0;
	for(i = 0; i < 52; i++)
		card[i + 1] = i % 13 + 1;
	for(i = 0; i < 53; i++)
	{
		a = rand() % 53;
		temp = card[a];
		card[a] = card[i];
		card[i] = temp;
	}
	deal();
}

void parseRead(int playerNO[], char toRead[])
{
	int i = 0, j = 0, k = 0, count = 0, x;
	while(count < 4)
	{
		while((toRead[i] != ' ') && (toRead[i] != '\n'))
		{
			j++;
			i++;
		}
		char buffer[j + 1];
		for(x = i - j; x < i; x++)
			buffer[x - (i - j)] = toRead[x];
		buffer[x] = '\0';
		playerNO[k] = atoi(buffer);
		k++;
		i++;
		j = 0;
		count++;
	}
}
int parse(char buffer[])
{
	char temp[128] = "";
	char temp2[128] = "";
	int i = 2, k = 0;
	while(buffer[i] != ' ')
	{
		temp2[k] = buffer[i];
		i++;
		k++;
	}
	int x = 0;
	char key[k + 1];
	for(x = 0; x < k; x++)
		key[x] = temp2[x];
	key[k] = '\0';
	i++;
	int j = 0;
	while((buffer[i] != '\n') && (buffer[i] != ' '))
	{
		temp[j] = buffer[i];
		i++;
		j++;
	}
	char ret[j + 1];
	for(i = 0 ; i < j; i++)
		ret[i] = temp[i];
	ret[i] = '\0';
	return atoi(ret);
}
int parsekey(char buffer[])
{
	char temp[128] = "";
	char temp2[128] = "";
	int i = 2, k = 0;
	while(buffer[i] != ' ')
	{
		temp2[k] = buffer[i];
		i++;
		k++;
	}
	int x = 0;
	char key[k + 1];
	for(x = 0; x < k; x++)
		key[x] = temp2[x];
	key[k] = '\0';
	if(buffer[0] == 'A')
	{
		if(atoi(key) == KEY[0])	return 0;
		else return 1;
	}
	else if(buffer[0] == 'B')
	{	if(atoi(key) == KEY[1])	return 0;
		else return 1;
	}
	else if(buffer[0] == 'C')
	{
		if(atoi(key) == KEY[2])	return 0;
		else return 1;
	}
	else if(buffer[0] == 'D')
	{
		if(atoi(key) == KEY[3])	return 0;
		else return 1;
	}
}
void stringHandle(int* count1, int* count2, FILE* fd, FILE* fd1, FILE* fd2)
{
	char buffer[128];
	fprintf(fd1, "< %d\n", *count1);
	fflush(fd1);
	char buffer1[128];
	do{
		fgets(buffer1, 128, fd);
	}while(parsekey(buffer1));
	fprintf(fd2, "> %d\n", parse(buffer1));
	fflush(fd2);
	char buffer2[128];
	do{
		fgets(buffer2, 128, fd);
	}while(parsekey(buffer2));
	fprintf(fd1, "%d\n", parse(buffer2));
	fflush(fd1);
	char buffer4[128];
	do{
		fgets(buffer4, 128, fd);
	}while(parsekey(buffer4));
	int ret = parse(buffer4);
	if(ret == 1)
		(*count2)--;
	else
		(*count2)++;
	(*count1)--;
}

int main(int argc, char* argv[])
{
	while(1){
	int judgeNum = atoi(argv[1]);
	pid_t pid;
	mode_t mode = 0700;
	int playerNO[4];
	int countcard[4];
	char toRead[128];
	read(STDIN_FILENO, toRead, 128);
	char* compbuf = "0 0 0 0\n";
	if(strcmp(toRead, compbuf) == 0)
		break;
	parseRead(playerNO, toRead);
	
	char buf[20] = {"judge"}, buf1[20] = {"judge"}, buf2[20] = {"judge"}, buf3[20] = {"judge"}, buf4[20] = {"judge"};
	int i;
	//create fifo
	strcat(buf, argv[1]);
	strcat(buf, ".FIFO");
    mkfifo(buf, mode);
	FILE* fd = fopen(buf, "r+");
	strcat(buf1, argv[1]);
	strcat(buf1, "_A.FIFO");
	mkfifo(buf1, mode);
	FILE* fdA = fopen(buf1, "r+");                                             
	strcat(buf2, argv[1]);
	strcat(buf2, "_B.FIFO");
	mkfifo(buf2, mode);
	FILE* fdB = fopen(buf2, "r+");
	strcat(buf3, argv[1]);
	strcat(buf3, "_C.FIFO");
	mkfifo(buf3, mode);
	FILE* fdC = fopen(buf3, "r+");
	strcat(buf4, argv[1]);
	strcat(buf4, "_D.FIFO");
	mkfifo(buf4, mode);
	FILE* fdD = fopen(buf4, "r+");
	pid_t pidarray[4];
	//fork and exec player process
	for(i = 0; i < 4; i++)
	{
		if((pid = fork()) == -1)
			perror("fork error");
		else if(pid == 0)//child process
		{
			char s1[128], s2[128], s3[128];
			sprintf(s1, "%d", judgeNum);
			sprintf(s2, "%c", i + 'A');
			sprintf(s3, "%d", KEY[i]);
			execlp("./player", "./player", s1, s2, s3, (char*) 0);
		}
		else
			pidarray[i] = pid;
	}
	shuffle();//shuffle and deal cards
	
	fprintf(fdA, "%s", cardA);
	fflush(fdA);
	fprintf(fdB, "%s", cardB);
	fflush(fdB);
	fprintf(fdC, "%s", cardC);
	fflush(fdC);
	fprintf(fdD, "%s", cardD);
	fflush(fdD);
	//get the number of cards form player
	for(i = 0; i < 4; i++)
	{
		char cardbuff[128];
		fgets(cardbuff, 128, fd);
		if(cardbuff[0] == 'A')
			countcard[0] = parse(cardbuff);
		else if(cardbuff[0] == 'B')
			countcard[1] = parse(cardbuff);
		else if(cardbuff[0] == 'C')
			countcard[2] = parse(cardbuff);
		else if(cardbuff[0] == 'D')
			countcard[3] = parse(cardbuff);
	}
	while(1)//play cards
	{
		char buffer[128];
		if(countcard[0] >= 1)
		{
			if(countcard[1] >= 1)
				stringHandle(&countcard[1], &countcard[0], fd, fdA, fdB);
			else if(countcard[2] >= 1)
				stringHandle(&countcard[2], &countcard[0], fd, fdA, fdC);
			else if(countcard[3] >= 1)
				stringHandle(&countcard[3], &countcard[0], fd, fdA, fdD);
			else//A has the joker
			{
				sprintf(buffer, "%d\n", playerNO[0]);
				write(STDOUT_FILENO, buffer, strlen(buffer) + 1);//tell organizer the loser is playerNO[0]
				break;
			}
		}
		if(countcard[1] >= 1)
		{
			if(countcard[2] >= 1)
				stringHandle(&countcard[2], &countcard[1], fd, fdB, fdC);
			else if(countcard[3] >= 1)
				stringHandle(&countcard[3], &countcard[1], fd, fdB, fdD);
			else if(countcard[0] >= 1)
				stringHandle(&countcard[0], &countcard[1], fd, fdB, fdA);
			else//B has the joker
			{
				sprintf(buffer, "%d\n", playerNO[1]);
				write(STDOUT_FILENO, buffer, strlen(buffer) + 1);//tell organizer the loser is playerNO[1]
				break;
			}
		}
		if(countcard[2] >= 1)
		{
			if(countcard[3] >= 1)
				stringHandle(&countcard[3], &countcard[2], fd, fdC, fdD);
			else if(countcard[0] >= 1)
				stringHandle(&countcard[0], &countcard[2], fd, fdC, fdA);
			else if(countcard[1] >= 1)
				stringHandle(&countcard[1], &countcard[2], fd, fdC, fdB);
			else//C has the joker
			{
				sprintf(buffer, "%d\n", playerNO[2]);
				write(STDOUT_FILENO, buffer, strlen(buffer) + 1);//tell organizer the loser is playerNO[2]
				break;
			}
		}
		if(countcard[3] >= 1)
		{
			if(countcard[0] >= 1)
				stringHandle(&countcard[0], &countcard[3], fd, fdD, fdA);
			else if(countcard[1] >= 1)
				stringHandle(&countcard[1], &countcard[3], fd, fdD, fdB);
			else if(countcard[2] >= 1)
				stringHandle(&countcard[2], &countcard[3], fd, fdD, fdC);
			else//D has the joker
			{
				sprintf(buffer, "%d\n", playerNO[3]);
				write(STDOUT_FILENO, buffer, strlen(buffer) + 1);//tell organizer the loser is playerNO[3]
				break;
			}
		}
	}
	unlink(buf);
	unlink(buf1);
	unlink(buf2);
	unlink(buf3);
	unlink(buf4);
	for(i = 0; i < 4; i++)
		kill(pidarray[i], 9);
	for(i = 0; i < 4; i++)
		wait(0);
	}//end of while
	exit(0);
}