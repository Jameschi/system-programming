#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<string.h>
#include<unistd.h>
#include<libgen.h>
#include<dirent.h>
#include<pwd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/time.h>
#include<sys/select.h>
#include<limits.h>
pid_t pid;
FILE* receiver_log;
int veryurgent = 1, urgent = 1, ordinary = 1;
static void sigint_handler(int signo)
{
	fprintf(receiver_log, "terminate");
	fflush(receiver_log);
	kill(pid, 9);
	fclose(receiver_log);
	_exit(0);
}
static void sigusr2_handler(int signo)
{
	fprintf(receiver_log, "receive 2 %d\n", veryurgent);
	fflush(receiver_log);
	struct timespec tp, ret;
	tp.tv_sec = ret.tv_sec = 0;
	tp.tv_nsec = ret.tv_nsec = 200000000;
	int n = -1;
	while(n == -1)
	{
		n = nanosleep(&tp, &ret);
		tp.tv_nsec = ret.tv_nsec;
	}
	kill(pid, SIGUSR2);
	fprintf(receiver_log, "finish 2 %d\n", veryurgent);
	fflush(receiver_log);
	veryurgent++;
}
static void sigusr1_handler(int signo)
{
	fprintf(receiver_log, "receive 1 %d\n", urgent);
	fflush(receiver_log);
	struct timespec tp, ret;
	tp.tv_sec = ret.tv_sec = 0;
	tp.tv_nsec = ret.tv_nsec = 500000000;
	int n = -1;
	while(n == -1)
	{
		n = nanosleep(&tp, &ret);
		tp.tv_nsec = ret.tv_nsec;
	}
	kill(pid, SIGUSR1);
	fprintf(receiver_log, "finish 1 %d\n", urgent);
	fflush(receiver_log);
	urgent++;
}
int main(int argc, char* argv[])
{
	//install signal handler sigusr2
	struct sigaction act2;
	sigemptyset(&act2.sa_mask);
	sigaddset(&act2.sa_mask, SIGUSR1);
	act2.sa_handler = sigusr2_handler;
	act2.sa_flags = SA_RESTART;
	sigaction(SIGUSR2, &act2, NULL);
	
	//install signal handler sigurs1
	struct sigaction act1;
	sigemptyset(&act1.sa_mask);
	act1.sa_handler = sigusr1_handler;
	act1.sa_flags = SA_RESTART;
	sigaction(SIGUSR1, &act1, NULL);
	
	//install signal handler sigint
	struct sigaction act0;
	sigemptyset(&act0.sa_mask);
	sigaddset(&act0.sa_mask, SIGUSR1);
	sigaddset(&act0.sa_mask, SIGUSR2);
	act0.sa_handler = sigint_handler;
	sigaction(SIGINT, &act0, NULL);
	
	//open file
	receiver_log = fopen("receiver_log", "w");
	
	int fdin[2], fdout[2];
	if((pipe(fdin) == -1) || (pipe(fdout) == -1))
		perror("pipe error");
	if((pid = fork()) == -1)
		perror("fork error");
	else if(pid == 0)//child
	{
		close(fdin[1]);
		close(fdout[0]);
		dup2(fdout[1], STDOUT_FILENO);
		dup2(fdin[0], STDIN_FILENO);
		close(fdout[1]);
		close(fdin[0]);
		execlp("./sender", "./sender", argv[1], (char*) 0);
	}
	else if(pid > 0)//parent
	{
		close(fdin[0]);
		close(fdout[1]);
	}
	
	char buffer[10];
	int n, sum;
	while(1)
	{
		int n = read(fdout[0], buffer, 9);
		buffer[9] = '\0';
		if(n == 0)
		{
			fprintf(receiver_log, "terminate");
			fflush(receiver_log);
			fclose(receiver_log);
			return 0;
		}
		if(strcmp(buffer, "ordinary\n") == 0)//receive ordinary mail
		{
			fprintf(receiver_log, "receive 0 %d\n", ordinary);
			fflush(receiver_log);
			struct timespec tp, ret;
			tp.tv_sec = ret.tv_sec = 0;
			tp.tv_nsec = ret.tv_nsec = 999999999;
			int n = -1;
			while(n == -1)
			{
				n = nanosleep(&tp, &ret);
				tp.tv_nsec = ret.tv_nsec;
			}
			kill(pid, SIGINT);//tell sender that ordinary mail is done
			fprintf(receiver_log, "finish 0 %d\n", ordinary);
			fflush(receiver_log);
			ordinary++;
		}
		
	}
}



