These programs implement hw3 using signals and pipe.
Note that nanosleep is used to simulate processing time to get a higher resolution than usleep.
Read and write will restart if they are interrupted by signals.
Setitimer and a flag are used to simulate the sending time of mails.
