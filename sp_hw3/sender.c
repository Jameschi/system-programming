#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<string.h>
#include<unistd.h>
#include<libgen.h>
#include<dirent.h>
#include<pwd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/time.h>
#include <time.h>
#include<sys/select.h>
#include<limits.h>
int i, count, flag, veryurgent = 1, Urgent = 1, Ordinary = 1, Ordinarysend = 1;
int indicateO, indicateU, indicateVU;
struct itimerval value, ovalue;
int priority[50], Time[50];
pid_t pid;
FILE* sender_log;
unsigned long long very, urgent, ordinary;
static void sigusr2_handler(int signo)
{
	struct timespec tsp;
	clock_gettime(CLOCK_REALTIME, &tsp);
	if(tsp.tv_nsec + tsp.tv_sec * 1000000000 - very > 300000000)
	{
		fprintf(sender_log, "timeout");
		fflush(sender_log);
		fclose(sender_log);
		_exit(0);
	}
	else
	{
		fprintf(sender_log, "finish 2 %d\n", veryurgent);
		fflush(sender_log);
		indicateVU = 0;
		veryurgent++;
	}
}	
static void sigusr1_handler(int signo)
{
	struct timespec tsp;
	clock_gettime(CLOCK_REALTIME, &tsp);
	if(tsp.tv_nsec + tsp.tv_sec * 1000000000- urgent > 1000000000)
	{
		fprintf(sender_log, "timeout");
		fflush(sender_log);
		fclose(sender_log);
		_exit(0);
	}
	else
	{
		fprintf(sender_log, "finish 1 %d\n", Urgent);
		fflush(sender_log);
		indicateU = 0;
		Urgent++;
	}
}	
static void sigint_handler(int signo)
{
	fprintf(sender_log, "finish 0 %d\n", Ordinary);
	fflush(sender_log);
	indicateO--;
	Ordinary++;
}	
static void alarm_handler(int signo)
{
	flag = 1;
}
int main(int argc, char* argv[])
{
	pid = getppid();
	FILE* test_data = fopen(argv[1], "r");
	sender_log = fopen("sender_log", "w");
	signal(SIGALRM, alarm_handler);
	signal(SIGINT, sigint_handler);
	signal(SIGUSR1, sigusr1_handler);
	signal(SIGUSR2, sigusr2_handler);
	
	//read in data
	while(!feof(test_data))
	{
		fscanf(test_data, "%d %d", &priority[count], &Time[count]);
		count++;
	}
	count--;
	fclose(test_data);
	value.it_value.tv_sec = 0;
	value.it_value.tv_usec = 1;
	value.it_interval.tv_sec = 0;
	value.it_interval.tv_usec = 0;	
	setitimer(ITIMER_REAL, &value, &ovalue);
	i = 0;
	while(1)//send message to receiver
	{
		switch(priority[i])
		{
			struct timespec tsp;
			case 0:
				write(STDOUT_FILENO, "ordinary\n", 9);
				clock_gettime(CLOCK_REALTIME, &tsp);
				ordinary = tsp.tv_nsec + tsp.tv_sec * 1000000000;
				fprintf(sender_log, "send 0 %d\n", Ordinarysend);
				Ordinarysend++;
				fflush(sender_log);
				indicateO++;
				break;
			case 1:
				kill(pid, SIGUSR1);
				clock_gettime(CLOCK_REALTIME, &tsp);
				urgent = tsp.tv_nsec + tsp.tv_sec * 1000000000;
				fprintf(sender_log, "send 1 %d\n", Urgent);
				fflush(sender_log);
				indicateU = 1;
				break;
			case 2:
				kill(pid, SIGUSR2);
				clock_gettime(CLOCK_REALTIME, &tsp);
				very = tsp.tv_nsec + tsp.tv_sec * 1000000000;
				fprintf(sender_log, "send 2 %d\n", veryurgent);
				fflush(sender_log);
				indicateVU = 1;
				break;
			default:
				break;
		}
		i++;
		if(i >= count)
			break;
		flag = 0;
		value.it_value.tv_usec = (Time[i] - Time[i - 1]) * 100000;
		setitimer(ITIMER_REAL, &value, &ovalue);
		while(flag != 1)
			pause();
	}
	while((indicateO) != 0 || (indicateU) != 0 || (indicateVU) != 0);
		fclose(sender_log);
	return 0;
	
}
