#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
int* arr;
/*structures to hold arguments to pass into merge sort*/
typedef struct{
	int head, tail;
}Arguments1;
typedef struct{
	int leftHead, leftTail, rightHead, rightTail;
}Arguments2;
/*merges the two arrays together in sorted order*/
static void *merge(void* arg)
{
	Arguments2* args = (Arguments2*) arg;
	int* temp = (int*)malloc((args -> rightTail - args -> leftHead) * sizeof(int));
	int status, lock = 0;
	int i = args -> leftHead, j = args -> rightHead, k = 0, duplicate = 0;
	while(i < args -> leftTail && j < args -> rightTail)
	{	
		if(arr[i] <= arr[j])
		{
			if(arr[i] == arr[j])
			{
				if(lock == 0)
				{
					status = arr[i];
					lock = 1;
					duplicate++;
				}
				if(status != arr[i])
				{
					duplicate++;
					status = arr[i];
				}
			}
			temp[k++] = arr[i++];
		}
		else
			temp[k++] = arr[j++];
	}
	if(i == args -> leftTail && j != args -> rightTail)
		for(i = j; i < args -> rightTail; i++)
			temp[k++] = arr[i];
	else if(i != args -> leftTail && j == args -> rightTail)
		for(j = i; j < args -> leftTail; j++)
			temp[k++] = arr[j];
	for(i = args -> leftHead; i < args -> rightTail; i++)
		arr[i] = temp[i - args -> leftHead];
	printf("Merged %d and %d elements with %d duplicates.\n", args -> leftTail - args -> leftHead, args -> rightTail - args -> rightHead, duplicate);
	free(temp);
	pthread_exit(0);
}
static int compare(const void* a, const void* b)
{
	return (*(int*)a - *(int*)b);
} 
static void *sort(void* arg)
{
	qsort(&arr[((Arguments1*)arg) -> head], ((Arguments1*)arg) -> tail - ((Arguments1*)arg) -> head, sizeof(int), compare);
	printf("Sorted %d elements.\n", ((Arguments1*)arg) -> tail - ((Arguments1*)arg) -> head);
	pthread_exit(0);
} 

int main(int argc, char* argv[])
{
	int partition = atoi(argv[1]);
	pthread_t *tids;
	Arguments1* args;
	int len;
	scanf("%d", &len);
	int thread_ct;
	if(len % partition == 0)
		thread_ct = len / partition;
	else
		thread_ct = len / partition + 1;
	arr = (int*)malloc(len * sizeof(int));
	tids = (pthread_t*)malloc(thread_ct * sizeof(pthread_t));
	args = (Arguments1*)malloc(thread_ct * sizeof(Arguments1));
	if(!tids || !args || !arr)
		perror("memory allocation error");
	int i;
	for(i = 0; i < len; i++)//read in data
		scanf("%d", &arr[i]);
	if(len % partition == 0)//every partition is equal
	{
		/* spawns then joins threads */
		for(i = 0; i < thread_ct; i++)
		{
			args[i].head = partition * i;
			args[i].tail = partition * (i + 1);
			if(pthread_create(&tids[i], NULL, &sort, &args[i]))
				perror("pthread_create() failed");
		}
	}
	else//the last one has fewer elements
	{
		for(i = 0; i < thread_ct - 1; i++)
		{
			args[i].head = partition * i;
			args[i].tail = partition * (i + 1);
			if(pthread_create(&tids[i], NULL, &sort, &args[i]))
				perror("pthread_create() failed");
		}
		args[i].head = partition * i;
		args[i].tail = len;
		if(pthread_create(&tids[i], NULL, &sort, &args[i]))
				perror("pthread_create() failed");
	}
	for(i = 0; i < thread_ct; i++)
		if(pthread_join(tids[i], NULL))
			perror("pthread_join() failed");
	/* merges the work done by threads if merging is necessary */
	int size = partition;
	Arguments2* args2;
	pthread_t* tid;
	if(thread_ct > 1) 
	{
		int num = thread_ct;
		while(num != 1)
		{
			args2 = (Arguments2*)malloc((num / 2) * sizeof(Arguments2));
			tid = (pthread_t*)malloc((num / 2) * sizeof(pthread_t));
			for(i = 0; i < num / 2; i++)//allocate arguments2
			{
				args2[i].leftHead = size * i * 2;
				args2[i].leftTail = size * (i * 2 + 1);
				if(size * (i * 2 + 2) <= len)
				{
					args2[i].rightHead = size * (i * 2 + 1);
					args2[i].rightTail = size * (i * 2 + 2);
				}
				else
				{
					args2[i].rightHead = size * (i * 2 + 1);
					args2[i].rightTail = len;
				}
			}
			for(i = 0; i < num / 2; i++)
				if(pthread_create(&tid[i], NULL, &merge, &args2[i]))
					exit(0);
			for(i = 0; i < num / 2; i++)
				if(pthread_join(tid[i], NULL))
					exit(0);
			num -= (num / 2);
			size *= 2;
			free(args2);
			free(tid);
		}
	} /* if only one thread, no merging necessary */
	for(i = 0; i < len; i++)
		printf("%d ", arr[i]);
	printf("\n");
}
