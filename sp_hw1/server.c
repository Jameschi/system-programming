#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define ERR_EXIT(a) { perror(a); exit(1); }

typedef struct 
{
    char hostname[512];  // server's hostname
    unsigned short port;  // port to listen
    int listen_fd;  // fd to wait for a new connection
} server;

typedef struct 
{
    char host[512];  // client's host
    int conn_fd;  // fd to talk with client
    char buf[512];  // data sent by/to client
    size_t buf_len;  // bytes used by buf
	int account;
} request;

typedef struct 
{
    int id;
    int money;
} Account;

server svr;  // server
request* requestP = NULL;  // point to a list of requests
int maxfd;  // size of open file descriptor table, size of request list

const char* accept_read_header = "ACCEPT_FROM_READ";
const char* accept_write_header = "ACCEPT_FROM_WRITE";
const char* reject_header = "REJECT\n";

static void init_server(unsigned short port);
// initailize a server, exit for error

static void init_request(request* reqP);
// initailize a request instance

static int handle_read(request* reqP);
// return 0: socket ended, request done.
// return 1: success, message (without header) got this time is in reqP->buf with reqP->buf_len bytes. read more until got <= 0.
// It's guaranteed that the header would be correctly set after the first read.
// error code:
// -1: client connection error
static int lock_reg(int fd, int type, off_t offset, int whence, off_t len)
{
    struct flock    lock;
    lock.l_type = type;     /* F_RDLCK, F_WRLCK, F_UNLCK */
    lock.l_start = offset;  /* byte offset, relative to l_whence */
    lock.l_whence = whence; /* SEEK_SET, SEEK_CUR, SEEK_END */
    lock.l_len = len;       /* #bytes (0 means to EOF) */
    return(fcntl(fd, F_SETLK, &lock));
}
int main(int argc, char** argv) 
{
    int i, ret;
	int indicate[21];
    struct sockaddr_in cliaddr;  // used by accept()
    int clilen;
    int conn_fd;  // fd for a new connection with client
    int file_fd;  // fd for file that we open for reading
    char buf[512];
    int buf_len;
    // Parse args.
    if (argc != 2) 
	{
        fprintf(stderr, "usage: %s [port]\n", argv[0]);
        exit(1);
    }
    // Initialize server
    init_server((unsigned short) atoi(argv[1]));

    // Get file descripter table size and initize request table
    maxfd = getdtablesize();

    requestP = (request*) malloc(sizeof(request) * maxfd);
    if (requestP == NULL) 
	{
        ERR_EXIT("out of memory allocating all requests");
    }
    for (i = 0; i < maxfd; i++) 
	{
        init_request(&requestP[i]);
    }
    requestP[svr.listen_fd].conn_fd = svr.listen_fd;
    strcpy(requestP[svr.listen_fd].host, svr.hostname);
    fprintf(stderr, "\nstarting on %.80s, port %d, fd %d, maxconn %d...\n", svr.hostname, svr.port, svr.listen_fd, maxfd);
	
	int *money;
    file_fd = open("./account_info",O_NONBLOCK | O_RDWR);
    money = (int*) malloc (sizeof(int));
    fd_set work_fd;
    FD_ZERO(&work_fd);
    FD_SET(svr.listen_fd, &work_fd);
	
    while (1) 
	{
        fd_set read_work_fd;
        memcpy(&read_work_fd, &work_fd, sizeof(work_fd));
        select(maxfd, &read_work_fd, NULL, NULL, NULL);//IO multiplexing

        for(i = 0; i < maxfd; i++)
        {
            if(requestP[i].conn_fd == -1)
            	continue;
			if(FD_ISSET(requestP[i].conn_fd, &read_work_fd))
			{
                if(requestP[i].conn_fd == svr.listen_fd)
				{
                    clilen = sizeof(cliaddr);
                    conn_fd = accept(svr.listen_fd, (struct sockaddr*)&cliaddr, (socklen_t*)&clilen);
                
                    if (conn_fd < 0) 
					{
                        if (errno == EINTR || errno == EAGAIN) continue;  // try again
                        if (errno == ENFILE) {
                            (void) fprintf(stderr, "out of file descriptor table ... (maxconn %d)\n", maxfd);
                            continue;
                        }
                        ERR_EXIT("accept")
                    }
                    requestP[conn_fd].conn_fd = conn_fd;
                    strcpy(requestP[conn_fd].host, inet_ntoa(cliaddr.sin_addr));
                    fprintf(stderr, "getting a new request... fd %d from %s\n", conn_fd, requestP[conn_fd].host);
                    FD_SET(conn_fd, &work_fd);
                }
                else
				{
					ret = handle_read(&requestP[i]); // parse data from client to requestP[conn_fd].buf
                    if (ret < 0) 
					{
                        fprintf(stderr, "bad request from %s\n", requestP[conn_fd].host);
                        continue;
                    }
                    else if (ret > 0)
					{                      
#ifdef READ_SERVER
						int x = lock_reg(file_fd, F_RDLCK, (atoi(requestP[i].buf) - 1) * 8 + 4, SEEK_SET, sizeof(int));//lock	
						if(x == -1)
						{
							char* stringbuf = "This account is occupied.\n";
							write(requestP[i].conn_fd, stringbuf, strlen(stringbuf));
							close(requestP[i].conn_fd);
							FD_CLR(requestP[i].conn_fd, &work_fd);
							init_request(&requestP[i]);
							continue;
						}
						lseek(file_fd, (atoi(requestP[i].buf) - 1) * 8 + 4, SEEK_SET);
						read(file_fd, money, sizeof(int));
						sprintf(buf,"%s: %d\n","Balance", money[0]);
						write(requestP[i].conn_fd,buf,strlen(buf));
						lock_reg(file_fd, F_UNLCK, (atoi(requestP[i].buf) - 1) * 8 + 4, SEEK_SET, sizeof(int));//unlock	
						close(requestP[i].conn_fd);
						FD_CLR(requestP[i].conn_fd, &work_fd);
						init_request(&requestP[i]);						
#else
                        if (requestP[i].account == 0)
						{
							int x = lock_reg(file_fd, F_WRLCK, (atoi(requestP[i].buf) - 1) * 8 + 4, SEEK_SET, sizeof(int));//lock
							if(x == -1)//multi server
							{
								char* stringbuf = "This account is occupied.\n";
								write(requestP[i].conn_fd, stringbuf, strlen(stringbuf));
								close(requestP[i].conn_fd);
								FD_CLR(requestP[i].conn_fd, &work_fd);
								init_request(&requestP[i]);
								continue;
							}
							if(indicate[atoi(requestP[i].buf)] != 1)//same server
							{
								char* stringbuf = "This account is available.\n";
								write(requestP[i].conn_fd, stringbuf, strlen(stringbuf));
								requestP[i].account = atoi(requestP[i].buf);
								indicate[atoi(requestP[i].buf)] = 1;
							}
							else
							{
								char* stringbuf = "This account is occupied.\n";
								write(requestP[i].conn_fd, stringbuf, strlen(stringbuf));
								close(requestP[i].conn_fd);
								FD_CLR(requestP[i].conn_fd, &work_fd);
								init_request(&requestP[i]);
							}
						}
						else
						{ 								
								lseek(file_fd, (requestP[i].account - 1) * 8 + 4, SEEK_SET);
								read(file_fd, money, sizeof(int));
								char Modstring[strlen(requestP[i].buf) - 1];
								int j = 1;
								while(requestP[i].buf[j] != '\0')
								{
									Modstring[j - 1] = requestP[i].buf[j];
									j++;
								}//parse the string, remove the sign
								int temp = atoi(Modstring);
								if(requestP[i].buf[0] == '+')
								{
									temp += *money;
									lseek(file_fd, (requestP[i].account - 1) * 8 + 4, SEEK_SET);
									write(file_fd, &temp, sizeof(int));
								}
								else
								{
									temp = *money - temp;
									if(temp < 0)
									{
										char* stringbuf = "Operation fail.\n";
										write(requestP[i].conn_fd, stringbuf, strlen(stringbuf));
									}
									else
									{
										lseek(file_fd, (requestP[i].account - 1) * 8 + 4, SEEK_SET);
										write(file_fd, &temp, sizeof(int));
									}
								}
								indicate[requestP[i].account] = 0;
								lock_reg(file_fd, F_UNLCK, (requestP[i].account - 1) * 8 + 4, SEEK_SET, sizeof(int));
								close(requestP[i].conn_fd);
								FD_CLR(requestP[i].conn_fd, &work_fd);
								init_request(&requestP[i]);		
                        }
#endif
                    }
                    else exit(1);				
                }
            }
        }
    }
    free(requestP);
    return 0;
}


// ======================================================================================================
// You don't need to know how the following codes are working
#include <fcntl.h>

static void* e_malloc(size_t size);


static void init_request(request* reqP) {
    reqP->conn_fd = -1;
    reqP->buf_len = 0;
    reqP->account = 0;
}

// return 0: socket ended, request done.
// return 1: success, message (without header) got this time is in reqP->buf with reqP->buf_len bytes. read more until got <= 0.
// It's guaranteed that the header would be correctly set after the first read.
// error code:
// -1: client connection error
static int handle_read(request* reqP) {
    int r;
    char buf[512];

    // Read in request from client
    r = read(reqP->conn_fd, buf, sizeof(buf));
    if (r < 0) return -1;
    if (r == 0) return 0;
	char* p1 = strstr(buf, "\015\012");
	int newline_len = 2;
	// be careful that in Windows, line ends with \015\012
	if (p1 == NULL) {
		p1 = strstr(buf, "\012");
		newline_len = 1;
		if (p1 == NULL) {
			ERR_EXIT("this really should not happen...");
		}
	}
	size_t len = p1 - buf + 1;
	memmove(reqP->buf, buf, len);
	reqP->buf[len - 1] = '\0';
	reqP->buf_len = len-1;
    return 1;
}

static void init_server(unsigned short port) {
    struct sockaddr_in servaddr;
    int tmp;

    gethostname(svr.hostname, sizeof(svr.hostname));
    svr.port = port;

    svr.listen_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (svr.listen_fd < 0) ERR_EXIT("socket");

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);
    tmp = 1;
    if (setsockopt(svr.listen_fd, SOL_SOCKET, SO_REUSEADDR, (void*)&tmp, sizeof(tmp)) < 0) {
        ERR_EXIT("setsockopt");
    }
    if (bind(svr.listen_fd, (struct sockaddr*)&servaddr, sizeof(servaddr)) < 0) {
        ERR_EXIT("bind");
    }
    if (listen(svr.listen_fd, 1024) < 0) {
        ERR_EXIT("listen");
    }
}

static void* e_malloc(size_t size) {
    void* ptr;

    ptr = malloc(size);
    if (ptr == NULL) ERR_EXIT("out of memory");
    return ptr;
}
